package com.rizalramadhan_10181053.praktikumpapb.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.rizalramadhan_10181053.praktikumpapb.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}